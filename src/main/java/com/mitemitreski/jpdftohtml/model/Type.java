package com.mitemitreski.jpdftohtml.model;

/**
 * Supported element types.
 */
public enum Type {
  UNKNOWN,
  H1,
  H2,
  H3,
  UL_EL,
  OL_EL
}
