package com.mitemitreski.jpdftohtml.model;

import java.util.List;

/**
 * Structured document representation.
 */
public class Document {

  private final List<Element> elementList;

  /**
   * Initialize  document using element list.
   *
   * @param elementList non null list of elements.
   */
  public Document(List<Element> elementList) {
    this.elementList = elementList;
  }

  public List<Element> getElementList() {
    return elementList;
  }
}
