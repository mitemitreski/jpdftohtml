package com.mitemitreski.jpdftohtml.model;

/**
 * Document element representation.
 */
public class Element {


  private final String value;
  private final Type type;

  /**
   * Generic element
   *
   * @param value text value
   */
  public Element(String value) {
    this.value = value;
    this.type = Type.UNKNOWN;
  }

  /**
   * Element with known type
   *
   * @param value text value
   * @param type  semantic type
   */
  public Element(String value, Type type) {
    this.value = value;
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public Type getType() {
    return type;
  }
}
