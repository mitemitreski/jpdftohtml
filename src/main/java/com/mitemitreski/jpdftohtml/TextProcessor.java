package com.mitemitreski.jpdftohtml;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.mitemitreski.jpdftohtml.model.Document;
import com.mitemitreski.jpdftohtml.model.Element;

import java.util.List;

/**
 * Process text to structured data.
 */
public class TextProcessor {

  private final TypeDetector typeDetector;
  private static final Splitter NEWLINE = Splitter.on("\n").trimResults();

  public TextProcessor(TypeDetector typeDetector) {
    this.typeDetector = typeDetector;
  }

  /**
   * Proccess data
   *
   * @param text
   * @return
   */
  public Document process(String text) {
    List<Element> elementList = Lists.newArrayList();
    Iterable<String> lines = NEWLINE.split(text);
    for (String line : lines) {
      elementList.add(new Element(line, typeDetector.resolve(line)));

    }

    return new Document(elementList);
  }
}
