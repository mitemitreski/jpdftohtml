package com.mitemitreski.jpdftohtml;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.IOException;
import java.io.InputStream;

/**
 * Reads texts from pdf input source.
 */
public class TextReader {

  /**
   * Read text.
   *
   * @param inputStream pdf input stream
   * @return read text
   * @throws java.io.IOException if reading failed
   */
  public String read(InputStream inputStream) throws IOException {
    PDDocument pdDoc = PDDocument.load(inputStream);
    PDDocument document = new PDDocument();
    PDFTextStripper textStripper = new PDFTextStripper();
    String text = textStripper.getText(pdDoc);
    return text;
  }

}
