package com.mitemitreski.jpdftohtml;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.mitemitreski.jpdftohtml.model.Document;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Main application
 */
public class App {

  public static void main(String[] args) throws IOException {

    if (args.length < 2) {
      System.out.println("call using 2 arguments source.pdf target.html");
    }

    TextReader reader = new TextReader();
    String text = reader.read(new FileInputStream(args[0]));
    System.out.println("Finished reading text");
    TextProcessor processor = new TextProcessor(new TypeDetector());
    Document document = processor.process(text);
    HTMLRenderer renderer = new HTMLRenderer();
    String html = renderer.render(document);
    System.out.println("Finished rendering text");
    Files.write(html, new File(args[1]), Charsets.UTF_8);
    System.out.println("DONE");
  }
}
