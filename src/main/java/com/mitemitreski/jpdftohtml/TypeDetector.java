package com.mitemitreski.jpdftohtml;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.mitemitreski.jpdftohtml.model.Type;

import java.util.Map;

/**
 * Matches text with semantic type.
 */
public class TypeDetector {

  Map<Type, Predicate<String>> typePredicateMap = ImmutableMap.<Type, Predicate<String>>of(
      Type.H1, new Predicate<String>() {
        @Override
        public boolean apply(String s) {
          //Match  "nodigit. anyting"
          return s.matches("^[A-Z]+\\.\\s.*$");
        }
      },
      Type.H2, new Predicate<String>() {
        @Override
        public boolean apply(String s) {
          //Match  "digit. anyting"
          return s.matches("^\\d+\\.\\s.*$");
        }
      },
      Type.H3, new Predicate<String>() {
        @Override
        public boolean apply(String s) {
          //Match  "digit.digit anyting"
          return s.matches("^\\d+\\.\\d+\\s.*$");
        }
      },
      Type.UL_EL, new Predicate<String>() {
        @Override
        public boolean apply(String s) {
          return s.matches("^\\s.*$");
        }
      },
      Type.OL_EL, new Predicate<String>() {
        @Override
        public boolean apply(String s) {
          return s.matches("^\\-\\s.*$");
        }
      }
  );


  /**
   * Reads text data and matches it to semanic type.
   *
   * @param text input line
   * @return Semantic type or unknown.
   */
  public Type resolve(String text) {
    if (text == null) {
      return Type.UNKNOWN;
    }

    for (Map.Entry<Type, Predicate<String>> entry : typePredicateMap.entrySet()) {
      if (entry.getValue().apply(text)) {
        return entry.getKey();
      }
    }

    return Type.UNKNOWN;
  }

}
