package com.mitemitreski.jpdftohtml;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.mitemitreski.jpdftohtml.model.Document;
import com.mitemitreski.jpdftohtml.model.Element;
import com.mitemitreski.jpdftohtml.model.Type;

import java.io.IOException;
import java.net.URL;

/**
 * Renders HTML from structured Document.
 */
public class HTMLRenderer {

  private static final String HTML_TEMPLATE = "template.html";

  /**
   * Renders Document to HTML using template
   *
   * @param document input
   * @return
   */
  public String render(Document document) throws IOException {
    URL url = Resources.getResource(HTML_TEMPLATE);
    String template = Resources.toString(url, Charsets.UTF_8);
    StringBuilder renderedText = new StringBuilder();
    Type lastElementsType = Type.UNKNOWN;
    for (Element element : document.getElementList()) {
      switch (element.getType()) {
        case H1:
          renderedText
              .append("<h1>")
              .append(element.getValue())
              .append("</h1>");
        case H2:
          renderedText
              .append("<h2>")
              .append(element.getValue())
              .append("</h2>");
          break;
        case H3:
          renderedText
              .append("<h2>")
              .append(element.getValue())
              .append("</h2>");
          break;

        case UL_EL:
        case OL_EL:
          renderedText
              .append("<li>")
              .append("&nbsp;" + element.getValue().substring(2))
              .append("</li>");
          break;
        case UNKNOWN:
          renderedText.append(element.getValue());
          break;
        default:
          break;
      }
      appendULIfNeeded(renderedText, lastElementsType, element);
      appendOLIfNeeded(renderedText, lastElementsType, element);

      lastElementsType = element.getType();
    }

    String out = template.replace("RENDER_TEXT", renderedText.toString());
    return out;

  }

  private void appendULIfNeeded(StringBuilder renderedText, Type lastElementsType, Element element) {
    if (lastElementsType != Type.UL_EL && element.getType() == Type.UL_EL) {
      renderedText.append("<ul>");

    }
    if (lastElementsType == Type.UL_EL && element.getType() != Type.UL_EL) {
      renderedText.append("</ul>");

    }
  }

  private void appendOLIfNeeded(StringBuilder renderedText, Type lastElementsType, Element element) {
    if (lastElementsType != Type.OL_EL && element.getType() == Type.OL_EL) {
      renderedText.append("<ol>");

    }
    if (lastElementsType == Type.OL_EL && element.getType() != Type.OL_EL) {
      renderedText.append("</ol>");

    }
  }

}
