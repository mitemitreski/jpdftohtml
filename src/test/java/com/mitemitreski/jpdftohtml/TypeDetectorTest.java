package com.mitemitreski.jpdftohtml;


import com.mitemitreski.jpdftohtml.model.Type;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TypeDetectorTest {

  TypeDetector detector = new TypeDetector();

  @Test
  public void resolve_blank_null() throws Exception {
    //given
    //when
    Type valNull = detector.resolve(null);
    Type valBlank = detector.resolve("");
    Type valEmpty = detector.resolve(" ");
    //then
    assertThat(valNull, is(Type.UNKNOWN));
    assertThat(valBlank, is(Type.UNKNOWN));
    assertThat(valEmpty, is(Type.UNKNOWN));
  }


  @Test
  public void should_resolve_h2() throws Exception {
    //given
    String text = "3. PHARMACEUTICAL FORM ";
    //when
    Type valEmpty = detector.resolve(text);
    //then
    assertThat(valEmpty, is(Type.H2));
  }



  @Test
  public void should_resolve_h3() throws Exception {
    //given
    String text = "4.8 Undesirable effects ";
    //when
    Type valEmpty = detector.resolve(text);
    //then
    assertThat(valEmpty, is(Type.H3));
  }
  @Test
  public void should_resolve_h1() throws Exception {
    //given
    String text = "A. MANUFACTURER(S) RESPONSIBLE FOR BATCH RELEASE  ";
    //when
    Type valEmpty = detector.resolve(text);
    //then
    assertThat(valEmpty, is(Type.H1));
  }


}