Scripts Available
=================

## Install 

We need maven 3.x and java 1.6+

Checkout project
`mvn clean install`
in the target folder
'java -jar    cli-1.0-SNAPSHOT.one-jar.jar SOURCE.pdf target.html`

For example : 
java -jar cli-1.0-SNAPSHOT.one-jar.jar /projects/personal/j-pdf-to-html/src/test/resources/xarelto.pdf /tmp/test2.html

##basic implementation 

 - H1,H2,H3,UL,OL

## impl details
 
PDF is parsed to plain text -> we match elements using Predicates -> we build semantic Document -> render semantic document to pdf
Template is available on classpath


## Known Issues 

- some false positives on H2, the matcher should make H1-3 only if the previous line was empty
- error handing/reporting is not implemented
- tables not implemented 
- majority of functionality is untested
- we should write to Std.OUT only in a case of verbose flag enabled not always
- Improvements can be done if we do not read the PDF all at 
- It is slow since performance was not considered at all
- It won't work on huge documents ex 1GB + since we have some duplication in memory and we read the PDF all at once
- Proper logging is missing
- Proper cli argument handling missing, ex Apache CLI




 